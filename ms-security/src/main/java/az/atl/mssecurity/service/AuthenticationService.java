package az.atl.mssecurity.service;

import az.atl.mssecurity.dao.entity.UserEntity;
import az.atl.mssecurity.dao.repository.UserRepository;
import az.atl.mssecurity.model.AuthenticationRequest;
import az.atl.mssecurity.model.AuthenticationResponse;
import az.atl.mssecurity.model.RegisterRequest;
import az.atl.mssecurity.model.RegisterResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public RegisterResponse register (RegisterRequest request){

        var isExist = repository.findByUsername(request.getUsername()).isPresent();
        if (isExist){
            throw new RuntimeException("User already exist");
        }

        var user = UserEntity.builder()
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(request.getRole()).build();

        var userEntity = repository.save(user);
        return RegisterResponse.buildRegisterDto(userEntity);

    }

    public AuthenticationResponse authenticate(AuthenticationRequest request){
        authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                    request.getUsername(),
                    request.getPassword()
            )
        );
        var user = repository.findByUsername(request.getUsername())
                .orElseThrow(()-> new RuntimeException("User not found!"));

        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }

}



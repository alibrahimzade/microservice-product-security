package az.atl.mssecurity.model.dto;

import az.atl.mssecurity.model.Role;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDto {
    Long id;

    String username;

    String password;
    @Enumerated(EnumType.STRING)
    Role role;
}

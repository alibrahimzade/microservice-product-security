package az.atl.mssecurity.model;

import az.atl.mssecurity.dao.entity.UserEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegisterResponse {

    String username;

    String password;

    Role role;

    public static  RegisterResponse buildRegisterDto(UserEntity userEntity){
        return RegisterResponse.builder()
                .username(userEntity.getUsername())
                .password(userEntity.getPassword())
                .role(userEntity.getRole()).build();
    }
}

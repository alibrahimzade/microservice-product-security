package az.atl.mssecurity.client;

import az.atl.mssecurity.model.dto.ProductDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@FeignClient(value = "productClient",url = "http://localhost:8083/product")
public interface ProductClient {
    @GetMapping("/all")
    List<ProductDto> getAllProducts(@RequestHeader("Authorization") String token);


    @PostMapping("/create")
    void createProduct(@RequestHeader("Authorization") String token , ProductDto productDto);
}

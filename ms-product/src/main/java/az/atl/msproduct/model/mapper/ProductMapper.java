package az.atl.msproduct.model.mapper;

import az.atl.msproduct.dao.entity.ProductEntity;
import az.atl.msproduct.model.dto.ProductDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);


    ProductDto buildEntityToDto(ProductEntity productEntity);
    List<ProductDto> buildEntityToDto(List<ProductEntity> productEntity);

    ProductEntity buildDtoToEntity(ProductDto productDto);


}

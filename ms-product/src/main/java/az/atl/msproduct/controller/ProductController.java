package az.atl.msproduct.controller;

import az.atl.msproduct.dao.entity.ProductEntity;
import az.atl.msproduct.model.dto.ProductDto;
import az.atl.msproduct.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @GetMapping("/all")
    public List<ProductDto> getAllProducts(){
        return productService.getAllProducts();
    }

    @GetMapping("/byId/{id}")
    public ProductDto getProductById(@PathVariable("id") Long id){
        return productService.getProductById(id);
    }

    @PostMapping("/create")
    public void createProduct(@RequestBody ProductDto productDto){
        productService.createProduct(productDto);
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteProductById(@PathVariable("id") Long id){
        productService.deleteById(id);
    }
}

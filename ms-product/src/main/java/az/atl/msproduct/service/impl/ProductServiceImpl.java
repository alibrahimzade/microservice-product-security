package az.atl.msproduct.service.impl;

import az.atl.msproduct.dao.entity.ProductEntity;
import az.atl.msproduct.dao.repository.ProductRepository;
import az.atl.msproduct.model.dto.ProductDto;
import az.atl.msproduct.model.mapper.ProductMapper;
import az.atl.msproduct.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    @Override
    public List<ProductDto> getAllProducts() {
        List<ProductEntity> all = productRepository.findAll();
        return ProductMapper.INSTANCE.buildEntityToDto(all);
    }

    @Override
    public void createProduct(ProductDto productDto) {
        ProductEntity productEntity = ProductMapper.INSTANCE.buildDtoToEntity(productDto);
        productRepository.save(productEntity);
    }

    @Override
    public ProductDto getProductById(Long id) {
        ProductEntity productEntity = productRepository.findById(id).get();
        return ProductMapper.INSTANCE.buildEntityToDto(productEntity);
    }

    @Override
    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }


}

package az.atl.msproduct.service;

import az.atl.msproduct.dao.entity.ProductEntity;
import az.atl.msproduct.model.dto.ProductDto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    List<ProductDto> getAllProducts();

    void createProduct(ProductDto productDto);

    ProductDto getProductById(Long id);

    void deleteById(Long id);

}
